(define (problem VRP)
	(:domain delivery)

	(:objects
		V1 V2 - vehicle
		RM MI VI TV - location
		F_VI F_TV - freight
	)

	(:init
		(at2 V1 RM) (at2 V2 MI)
		(at1 F_VI RM) (at1 F_VI MI) (at1 F_TV RM) (at1 F_TV MI) 
		(near RM MI) (near MI RM)
		(near RM VI) (near VI RM)
		(near RM TV) (near TV RM)
		(near MI VI) (near VI MI)
		(near MI TV) (near TV MI)
		(near VI TV) (near TV VI)
		(= (distance RM MI) 945) (= (distance MI RM) 945)
		(= (distance RM VI) 825) (= (distance VI RM) 825)
		(= (distance RM TV) 846) (= (distance TV RM) 846)
		(= (distance MI VI) 376) (= (distance VI MI) 376)
		(= (distance MI TV) 489) (= (distance TV MI) 489)
		(= (distance VI TV) 115) (= (distance TV VI) 115)
		(= (total_cost) 0)
	)

	(:goal (and
		(at2 V1 RM) (at2 V2 MI)
		(at1 F_VI VI)
		(at1 F_TV TV)
	))

	(:metric minimize (total-cost))
)