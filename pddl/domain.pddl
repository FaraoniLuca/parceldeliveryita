(define 
    (domain delivery)
    (:types location freight vehicle)

    (:predicates 
        (at1 ?f - freight ?l - location)
        (at2 ?v - vehicle ?l - location)
        (near ?l1 ?l2 - location)
        (in ?f - freight ?v - vehicle)
        (visited ?l - location)
    )

    (:functions
        (distance ?l1 ?l2 - location)
        (total-cost)
    )

    (:action move
        :parameters (?v - vehicle ?l1 ?l2 - location)
        :precondition (and
            (at2 ?v ?l1)
            (not (visited ?l2))
            (near ?l1 ?l2) 
        )
        :effect (and 
            (at2 ?v ?l2)
            (not (at2 ?v ?l1))
            (visited ?l2)
            (increase (total-cost) (distance ?l1 ?l2))
        )
    )

    (:action load
        :parameters (?f - freight ?v - vehicle ?l - location)
        :precondition (and 
            (at1 ?f ?l)
            (at2 ?v ?l)
        )
        :effect (and 
            (in ?f ?v)
            (not (at1 ?f ?l))
        )
    )

    (:action unload
        :parameters (?f - freight ?v - vehicle ?l - location)
        :precondition (and 
            (at2 ?v ?l)
            (in ?f ?v)
        )
        :effect (and 
            (at1 ?f ?l)
            (not (in ?f ?v))
        )
    )
)