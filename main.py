import sys
import configparser

from PyQt5.QtWidgets import QApplication
from gui.winwidget import WinWidget

if __name__ == "__main__":

    config = configparser.ConfigParser()
    try:
        config.read('settings.ini')
        config.sections()
    except (FileNotFoundError, IOError) as e:
        config['USER'] = {'STORAGES': []}
        config['STORAGES'] = {}

    app = QApplication(sys.argv)
    window = WinWidget(config)

    sys.exit(app.exec_())
