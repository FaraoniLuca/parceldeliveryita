#
#
# Implementation of QGraphicsView
#
# by Luca Faraoni

from PyQt5 import QtGui
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QGraphicsView, QGraphicsScene

class GraphicsView(QGraphicsView):

    def __init__(self, parent=None):
        super().__init__(parent)
        scene = QGraphicsScene(self)
        self.setScene(scene)
        self.__pixmap__item = self.scene().addPixmap(QPixmap())
        self.__pixmap__item.setZValue(-1)
    
    @property
    def pixmap(self):
        return self.__pixmap__item.pixmap()
    
    @pixmap.setter
    def pixmap(self, pixmap):
        self.__pixmap__item.setPixmap(pixmap)
        self.scene().setSceneRect(self.__pixmap__item.boundingRect())
    
    def resizeEvent(self, event):
        if not self.__pixmap__item.pixmap().isNull():
            self.fitInView(self.__pixmap__item)
        super().resizeEvent(event)

