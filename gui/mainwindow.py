
from PyQt5 import QtGui
from PyQt5.QtWidgets import QMainWindow, QWidget, QVBoxLayout

from .winwidget import WinWidget

class MainWindow(QMainWindow):

  def __init__(self, config, parent=None):

    super(MainWindow, self).__init__(parent)

    self.top    = 100
    self.left   = 100
    self.width  = 1500
    self.height = 900

    self.window = WinWidget(self, config)
    widget = QWidget()
    layout = QVBoxLayout(widget)
    layout.addWidget(self.window)

    self.setCentralWidget(widget)
    self.statusBar().showMessage('Ready')

    self.addMenuBar()

    self.setGeometry(self.top, self.left, self.width, self.height)
    self.setWindowTitle('parcelsDelivery')  
    self.show()
  
  def addMenuBar(self):
    menubar = self.menuBar() 
    fileMenu = menubar.addMenu('&File')
          