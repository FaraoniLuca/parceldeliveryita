#
# Implementation of a mainWindows
#
# by Luca Faraoni
import os
import csv
import subprocess
import numpy as np

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QPixmap, QPen
from PyQt5.QtWidgets import QLabel, QPushButton, QHBoxLayout, QVBoxLayout, QWidget, QInputDialog, QMessageBox

from .graphicsView import GraphicsView

class WinWidget(QWidget):
    def __init__(self, config):
        super().__init__()

        self.storage = config['USER']['STORAGE']
        self.capacity = config['STORAGES'][self.storage]

        self.path = []
        self.selectedCities = []
        self.edges = []
        self.nodes = load_cities()

        self.initUI()

    def initUI(self):

        self.view = GraphicsView(self)
        self.view.pixmap = QPixmap(
            os.path.join(os.getcwd(), "images/Italy.jpg"))
        self.view.setMaximumWidth(1000)

        scene = self.view.scene()
        font = QFont()
        font.setPixelSize(35)

        for node in self.nodes:
            label = scene.addText(node, font)
            label.setDefaultTextColor(Qt.black)
            label.setPos(self.nodes[node]['x'], self.nodes[node]['y'])

        welcomeMessage = "<h2>Welcome to DeliveryParcelIta.</h2> \
      <h3>Select cities where to bring parcels and add them to the path. <br> \
      When you have done, click on the 'Compute path' button.</h3>"
        welcomeLabel = QLabel(welcomeMessage)

        self.pathLabel = QLabel('Cities in the path: ' + str(self.path))
        self.selectedCitiesLabel = QLabel(
            'Selected cities: ' + str(self.selectedCities))
        #self.pathResult = QLabel('Result: ');

        addOrders = QPushButton('Add orders')
        addOrders.clicked.connect(self.addOrders)

        removeOrders = QPushButton('Remove orders')
        removeOrders.clicked.connect(self.removeOrders)

        clearOrders = QPushButton('Clear Path')
        clearOrders.clicked.connect(self.clearOrders)

        computePath = QPushButton('Compute path')
        computePath.clicked.connect(self.computePath)

        hbox = QHBoxLayout(self)
        hbox.addWidget(self.view)
        hbox.addStretch()

        vbox = QVBoxLayout()
        vbox.addWidget(welcomeLabel)
        vbox.addSpacing(50)
        vbox.addWidget(addOrders)
        vbox.addWidget(removeOrders)
        vbox.addWidget(clearOrders)
        vbox.addWidget(computePath)
        vbox.addSpacing(20)
        vbox.addWidget(self.selectedCitiesLabel)
        vbox.addSpacing(5)
        vbox.addWidget(self.pathLabel)
        vbox.addSpacing(10)
        # vbox.addWidget(self.pathResult)
        vbox.addStretch()

        hbox.addLayout(vbox)
        hbox.addStretch()

        self.setLayout(hbox)
        self.show()

        return

    def addOrders(self):
        text, ok = QInputDialog.getText(
            self, 'Add orders', 'Enter the orders to add: ')

        if ok:
            orders = text.split(',')
            for el in orders:
                order = el.strip()
                #valid, code = checkValidity(el)

                """ if not valid:
                    message = getMessageError(code)
                    QMessageBox.about(
                        self, "An error occurred on " + str(el), message)
                    continue """

                if not order in self.nodes:
                    QMessageBox.about(
                        self, "An error occurred on " + str(order), "Error 04, the given city is unknown")
                    continue

                if order in self.path:
                    QMessageBox.about(self, "An error occurred " + str(order),
                                      "Error 05, the given city is already in the path")
                    continue

                # self.path.append(city)
                self.selectedCities.append(order)
                #self.weights.append(order[1])

        self.selectedCitiesLabel.setText(
            'Selected cities: ' + str(self.selectedCities))
        return

    def removeOrders(self):
        text, ok = QInputDialog.getText(
            self, 'Remove orders', 'Enter the orders to remove')

        if ok:
            cities = text.split(',')
            for city in cities:
                try:
                    self.selectedCities.remove(city.strip())
                except ValueError as identifier:
                    QMessageBox.about(
                        self, "An error occurred", "The city " + city + "is not in the current path")
                    continue

        self.selectedCitiesLabel.setText(
            "Selected cities: " + str(self.selectedCities))
        #self.pathLabel.setText('Cities in the path: ' + str(self.path))

    def clearOrders(self):
        self.selectedCities = []
        self.selectedCitiesLabel.setText(
            "Selected cities: " + str(self.selectedCities))

    def computePath(self):
        self.path = ["RM", "MI"] + self.selectedCities
        f = open("pddl/problem.pddl", "w")
        f.write("(define (problem VRP)\n\t(:domain delivery)\n\n")

        objects = "\t(:objects\n\t\tV1 V2 - vehicle\n\t\t"
        locations = "RM MI "
        freights = ""
        freights_location = ""
        freights_destination = ""
        for el in self.selectedCities:
            locations += el + " "
            freight = "F_" + el + " "
            freights += freight

            freights_location += "(at1 " + freight + "RM) " + "(at1 " + freight + "MI) "
            freights_destination += "(at1 " + freight + el + ")\n\t\t"

        
        locations += "- location"
        freights += "- freight"
        objects += locations + "\n\t\t" + freights + "\n\t)\n\n\t"
        f.write(objects)

        init = "(:init\n\t\t(at2 V1 RM) (at2 V2 MI)\n\t\t"
        init += freights_location + "\n\t\t"
        
        near = ""
        distances = ""
        for i in range(len(self.path)):
            for j in range(i+1, len(self.path)):
                l1 = self.path[i]
                l2 = self.path[j]
                distance = int(np.hypot(self.nodes[l1]['x']-self.nodes[l2]['x'], self.nodes[l1]['y']-self.nodes[l2]['y']))

                near += "(near " + l1 + " " + l2 + ") (near " + l2 + " " + l1 + ")\n\t\t"
                distances += "(= (distance " + l1 + " " + l2 + ") " + str(distance) + ") (= (distance " + l2 + " " + l1 + ") " + str(distance) + ")\n\t\t"

        init += near + distances + "(= (total_cost) 0)\n\t)\n\n\t"
        f.write(init)
        
        goal = "(:goal (and\n\t\t(at2 V1 RM) (at2 V2 MI)\n\t\t"
        goal += freights_destination[:-1] + "))\n\n\t(:metric minimize (total-cost))\n)"
        f.write(goal)

        f.close()

        process = subprocess.Popen(["./downward/fast-downward.py", "pddl/domain.pddl", "pddl/problem.pddl", "--search", "astar(ipdb(), pruning=stubborn_sets_ec())"])
        while process.poll() is None:
            continue
        
        plan = []
        output = open("sas_plan", "r")

        for line in output:
            if line.startswith("(move"):
                x = line.split(" ")
                plan.append((x[2].upper(), x[3].upper()[:-2]))

        self.addArcs(plan)

        return

    def addArcs(self, plan):
        for edge in self.edges:
            self.view.scene().removeItem(edge)

        self.edges = []

        pen = QPen(Qt.black)
        pen.setWidth(5)
        pen.setCosmetic(True)

        for el in plan:
            start = self.nodes[el[0]]
            end = self.nodes[el[1]]
            edge = self.view.scene().addLine(
                start['x'], start['y'], end['x'], end['y'], pen)
            self.edges.append(edge)

        return

def load_cities():
  with open('cities.csv', mode='r') as csv_file:
    rows = csv.reader(csv_file)

    nodes = dict()
    for row in rows:
      node, x, y = row[0].split(' ')
      nodes[node] = {'x': int(x), 'y': int(y)}

  return nodes

